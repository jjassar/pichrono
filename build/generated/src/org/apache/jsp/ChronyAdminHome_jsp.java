package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;

public final class ChronyAdminHome_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
      out.write("<head>\n");
      out.write("   <title>Internet of Chrony Things</title>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("<link href=\"css/style.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/coin-slider.css\" />\n");
      out.write("<script type=\"text/javascript\" src=\"js/cufon-yui.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"js/cufon-titillium-600.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"js/jquery-1.4.2.min.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"js/script.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"js/coin-slider.min.js\"></script>\n");
      out.write("\n");
      out.write(" <script type=\"text/javascript\">\n");
      out.write("\n");
      out.write("function dsb()\n");
      out.write(" {\n");
      out.write("\n");
      out.write("if(window.history.forward(1) != null)\n");
      out.write("         window.history.forward(1);\n");
      out.write("     \n");
      out.write(" }\n");
      out.write("   window.history.forward(1);\n");
      out.write("     \n");
      out.write("</script>\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("<div class=\"main\">\n");
      out.write("  <div class=\"header\">\n");
      out.write("    <div class=\"header_resize\">\n");
      out.write("      <div class=\"logo\">\n");
      out.write("        <h1><a href=\"index.html\">Connected <span>Chronograph</span> <small>We deliver results</small></a></h1>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"searchform\">\n");
      out.write("        <form id=\"formsearch\" name=\"formsearch\" method=\"post\" action=\"#\">\n");
      out.write("          <span>\n");
      out.write("          <input name=\"editbox_search\" class=\"editbox_search\" id=\"editbox_search\" maxlength=\"80\" value=\"Search our ste:\" type=\"text\" />\n");
      out.write("          </span>\n");
      out.write("          <input name=\"button_search\" src=\"images/search.gif\" class=\"button_search\" type=\"image\" />\n");
      out.write("        </form>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"clr\"></div>\n");
      out.write("      <div class=\"slider\">\n");
      out.write("        <div id=\"coin-slider\"> <a href=\"#\"><img src=\"images/slide1.jpg\" width=\"940\" height=\"310\" alt=\"\" /> </a> <a href=\"#\"><img src=\"images/slide2.jpg\" width=\"940\" height=\"310\" alt=\"\" /> </a> <a href=\"#\"><img src=\"images/slide3.jpg\" width=\"940\" height=\"310\" alt=\"\" /> </a> </div>\n");
      out.write("        <div class=\"clr\"></div>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"clr\"></div>\n");
      out.write("      <div class=\"menu_nav\">\n");
      out.write("        <ul>\n");
      out.write("          <li class=\"active\"><a href=\"index.html\"><span>Home Page</span></a></li>\n");
      out.write("          <li><a href=\"RegisterChrony.jsp\"><span>Register Chrony</span></a></li>\n");
      out.write("          <li><a href=\"Livelogin.jsp\"><span>Live View</span></a></li>\n");
      out.write("          <li><a href=\"AdminLogin.jsp\"><span>Chrony Database</span></a></li>\n");
      out.write("          <li><a href=\"index.html\" onclick=\"return dsb()\"><span>Logout</span></a></li>\n");
      out.write("        </ul>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"clr\"></div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("  <div class=\"content\">\n");
      out.write("    <div class=\"content_resize\">\n");
      out.write("      <div class=\"mainbar\">\n");
      out.write("        <div class=\"article\">\n");
      out.write("          <h2><span>View Chrony Database </span>");
      out.print(session.getAttribute("username"));
      out.write("\n");
      out.write("          </h2>\n");
      out.write("          <div class=\"clr\"></div>\n");
      out.write("       </div>\n");
      out.write("        <div class=\"article\">\n");
      out.write("         \n");
      out.write("          <div class=\"clr\"></div>\n");
      out.write("          <style>\n");
      out.write("              .TFtable\n");
      out.write("              {\n");
      out.write("                  border:2px solid black;\n");
      out.write("                  background-color:#aaa;\n");
      out.write("                  width:100%;\n");
      out.write("                  border-collapse: collapse;\n");
      out.write("                  \n");
      out.write("              }\n");
      out.write("              .TFtable tr th{\n");
      out.write("                  border:2px solid black;\n");
      out.write("                  background-color:#363636;\n");
      out.write("                  font-size: 16px;\n");
      out.write("                  color: #FFF;\n");
      out.write("                  height: 40px;\n");
      out.write("              }\n");
      out.write("              .TFtable tr td{\n");
      out.write("                  border-bottom:1px solid white;\n");
      out.write("                  text-align:center;\n");
      out.write("                  font-size:12px;\n");
      out.write("                  height: 40px;\n");
      out.write("              }\n");
      out.write("              </style>\n");
      out.write("                      <table  class=\"TFtable\" >\n");
      out.write("                <tr>\n");
      out.write("                    <th> Sr. No. </th>\n");
      out.write("                    <th> Velocity </th>\n");
      out.write("                    <th> Time </th>\n");
      out.write("                    <th> Date </th>\n");
      out.write("        \n");
      out.write("                   \n");
      out.write("                </tr>\n");
      out.write("        \n");
      out.write("        \n");
      out.write("            ");

            int i=0;
            int j=2;
                Connection con=   factory.confac.getConnection();
                Statement stmt= con.createStatement();
               ResultSet rs= stmt.executeQuery("select * from velrecords where chserial='"+session.getAttribute("cn")+"'");
              while(rs.next())
              {
              i++;
                  if(j%2==0)
                  {
                
      out.write("\n");
      out.write("              <tr>\n");
      out.write("                    <td>");
      out.print(i);
      out.write("</td>\n");
      out.write("                    <td>");
      out.print(rs.getString(3));
      out.write(" </td>\n");
      out.write("                    <td>");
      out.print(rs.getString(4));
      out.write(" </td>\n");
      out.write("                    <td>");
      out.print(rs.getString(5));
      out.write(" </td>\n");
      out.write("                    \n");
      out.write("                </tr>\n");
      out.write("            ");

                  }
                  else
                  {
                     
      out.write("\n");
      out.write("                  <tr style=\"background-color:white\">\n");
      out.write("                    <td>");
      out.print(i);
      out.write("</td>\n");
      out.write("                    <td>");
      out.print(rs.getString(3));
      out.write(" </td>\n");
      out.write("                    <td>");
      out.print(rs.getString(4));
      out.write(" </td>\n");
      out.write("                    <td>");
      out.print(rs.getString(5));
      out.write(" </td>\n");
      out.write("                    \n");
      out.write("                </tr>\n");
      out.write("               \n");
      out.write("                  \n");
      out.write("                  ");

                  
                  }
                   j++;
              }
              
                  
      out.write("\n");
      out.write("                 \n");
      out.write("            </table> \n");
      out.write("          \n");
      out.write("          \n");
      out.write("          \n");
      out.write("          \n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    <div class=\"sidebar\">\n");
      out.write("        \n");
      out.write("        <div class=\"gadget\">\n");
      out.write("          <h2 class=\"star\"><span>Technologies Used</span></h2>\n");
      out.write("          <div class=\"clr\"></div>\n");
      out.write("          <ul class=\"ex_menu\">\n");
      out.write("            <li><a href=\"#\">JAVA Embedded</a><br />\n");
      out.write("              Embedded Suite</li>\n");
      out.write("            <li><a href=\"#\">Pi4j</a><br />\n");
      out.write("              Hardware Interface</li>\n");
      out.write("            <li><a href=\"#\">Raspbian</a><br />\n");
      out.write("              Wheezy based Linux</li>\n");
      out.write("              <li><a href=\"#\">Java Enterprise</a><br />\n");
      out.write("              Web Development</li>\n");
      out.write("            <li><a href=\"#\">Java Comet</a><br />\n");
      out.write("              Async Chat &amp;</li>\n");
      out.write("            <li><a href=\"#\">Google Glass</a><br />\n");
      out.write("             Time line</li>\n");
      out.write("          </ul>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"clr\"></div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("  <div class=\"fbg\">\n");
      out.write("    <div class=\"fbg_resize\">\n");
      out.write("      <div class=\"col c1\">\n");
      out.write("        <h2><span>Image</span> Gallery</h2>\n");
      out.write("        <a href=\"#\"><img src=\"images/emp.jpg\" width=\"75\" height=\"75\" alt=\"\" class=\"gal\" /></a> <a href=\"#\"><img src=\"images/emp2.png\" width=\"75\" height=\"75\" alt=\"\" class=\"gal\" /></a> <a href=\"#\"><img src=\"images/emp3.gif\" width=\"75\" height=\"75\" alt=\"\" class=\"gal\" /></a> <a href=\"#\"><img src=\"images/emp4.png\" width=\"75\" height=\"75\" alt=\"\" class=\"gal\" /></a> <a href=\"#\"><img src=\"images/emp5.png\" width=\"75\" height=\"75\" alt=\"\" class=\"gal\" /></a> <a href=\"#\"><img src=\"images/emp6.png\" width=\"75\" height=\"75\" alt=\"\" class=\"gal\" /></a> </div>\n");
      out.write("      <div class=\"col c2\">\n");
      out.write("          <h2><span>Hardware </span> Technologies</h2>\n");
      out.write("        <ul class=\"fbg_ul\">\n");
      out.write("          <li><a href=\"#\">Raspberry Pi</a></li>\n");
      out.write("          <li><a href=\"#\">Google IOIO</a></li>\n");
      out.write("          <li><a href=\"#\">Digital Circuits</a></li>\n");
      out.write("        </ul>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"col c3\">\n");
      out.write("        <h2><span>Contact</span> Us</h2>\n");
      out.write("        <p>Feel Free to connect with us. You can send Us your Query.</p>\n");
      out.write("        <p class=\"contact_info\"> <span>Address:</span>IOT Chrono<br />\n");
      out.write(" </p>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"clr\"></div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("  <div class=\"footer\">\n");
      out.write("    <div class=\"footer_resize\">\n");
      out.write("      <p class=\"lf\">&copy; <a href=\"#\">IOT Chrono</a>.</p>\n");
      out.write("      <p class=\"rf\">Design by<a href=\"#\">Charanjeet</a></p>\n");
      out.write("      <div style=\"clear:both;\"></div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("</div>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
